import 'package:flutter/material.dart';
import 'package:soluspopuli/formScreening.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  Widget build(context) {
    return MaterialApp(
      title: 'Solus Populi',
      home: Scaffold(
        body: FormScreening(),
      ),
    );
  }
}
