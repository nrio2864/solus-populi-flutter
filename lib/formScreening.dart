import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class FormScreening extends StatefulWidget {
  @override
  _FormScreeningState createState() => _FormScreeningState();
}

class _FormScreeningState extends State<FormScreening> {
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
  final GlobalKey<FormState> formKey = new GlobalKey<FormState>();
  bool isChecked = false;
  bool oxymeter = false;
  TextEditingController _controller = new TextEditingController();

  Map<String, bool> keluhanList = {
    'Demam': false,
    'Pilek': false,
    'Batuk': false,
    'Anosmia': false,
    'Sesak': false,
    'Muntah': false,
    'Diare': false
  };
  var holder_1 = [];

  getItem() {
    keluhanList.forEach((key, value) {
      if (value == true) {
        holder_1.add(key);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("Form"),
        backgroundColor: Colors.teal,
      ),
      body: new Container(
        padding: new EdgeInsets.all(16),
        child: new ListView(
          children: <Widget>[
            new Text(
              'Data Diri :',
              style: new TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 18.0,
              ),
            ),
            new Padding(padding: EdgeInsets.all(5)),
            new TextField(
              decoration: new InputDecoration(
                  labelText: "Nama",
                  border: new OutlineInputBorder(
                      borderRadius: new BorderRadius.circular(20.0))),
            ),
            new Text(
              'Apakah COVID / Bukan :',
              style: new TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 18.0,
              ),
            ),
            new Padding(padding: EdgeInsets.all(5)),
            CheckboxListTile(
              title: Text('Sudah Test'),
              value: isChecked,
              activeColor: Colors.deepPurpleAccent,
              onChanged: (value) {
                setState(() {
                  isChecked = value;
                });
              },
            ),
            new Padding(padding: EdgeInsets.all(5)),
            new Text(
              'Keluhan Utama :',
              style: new TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 18.0,
              ),
            ),
            new Padding(padding: EdgeInsets.all(5)),
            new TextField(
              decoration: new InputDecoration(
                  labelText: "Keluhan Utama",
                  border: new OutlineInputBorder(
                      borderRadius: new BorderRadius.circular(20.0))),
            ),
            new Padding(padding: EdgeInsets.all(5)),
            new Text(
              'Keluhan Lainya :',
              style: new TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 18.0,
              ),
            ),
            // new DropdownButton(
            //   items: [
            //     DropdownMenuItem(
            //       child: ListView(
            //         children: keluhanList.keys.map((String key) {
            //           return new CheckboxListTile(
            //             title: new Text(key),
            //             value: keluhanList[key],
            //             onChanged: (bool value) {
            //               setState(() {
            //                 keluhanList[key] = value;
            //               });
            //             },
            //           );
            //         }).toList(),
            //       ),
            //     )
            //   ],
            // ),
            Expanded(
                child: Column(
              children: keluhanList.keys.map((String key) {
                return new CheckboxListTile(
                  title: new Text(key),
                  value: keluhanList[key],
                  onChanged: (bool value) {
                    setState(() {
                      keluhanList[key] = value;
                    });
                  },
                );
              }).toList(),
            )),
            new Padding(padding: EdgeInsets.all(1)),
            new Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                new Text(
                  'Apakah memiliki Oxymeter :',
                  style: new TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 18.0,
                  ),
                ),
                new Checkbox(
                  // title: new Text(
                  //   'Apakah memiliki Oxymeter :',
                  //   style:
                  //       new TextStyle(fontWeight: FontWeight.bold, fontSize: 18.0),
                  // ),
                  value: oxymeter,
                  onChanged: (bool value) {
                    setState(() {
                      oxymeter = value;
                    });
                  },
                ),
              ],
            ),

            oxymeter
                ? new Container(
                    child: new Column(
                      children: <Widget>[
                        new TextField(
                          decoration: new InputDecoration(
                              labelText: "Saturasi O2",
                              border: new OutlineInputBorder(
                                  borderRadius:
                                      new BorderRadius.circular(20.0))),
                        ),
                        new Padding(padding: EdgeInsets.all(5)),
                        new TextField(
                          decoration: new InputDecoration(
                              labelText: "Denyut Nadi",
                              border: new OutlineInputBorder(
                                  borderRadius:
                                      new BorderRadius.circular(20.0))),
                        ),
                      ],
                    ),
                  )
                : new Container(),
            new Padding(padding: EdgeInsets.all(5)),
            new Text(
              'Suhu Tubuh :',
              style: new TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 18.0,
              ),
            ),
            new Padding(padding: EdgeInsets.all(5)),
            new TextField(
              decoration: new InputDecoration(
                  labelText: "Suhu Tubuh",
                  border: new OutlineInputBorder(
                      borderRadius: new BorderRadius.circular(20.0))),
            ),
            new Text(
              'Obat yang sudah dikonsumsi :',
              style: new TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 18.0,
              ),
            ),
            new Padding(padding: EdgeInsets.all(5)),
            new TextField(
              decoration: new InputDecoration(
                  labelText: "Obat",
                  border: new OutlineInputBorder(
                      borderRadius: new BorderRadius.circular(20.0))),
            ),
            new Padding(padding: EdgeInsets.all(5)),
            RaisedButton(
              child: Text(
                "Kirim",
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 18.0,
                ),
              ),
              color: Colors.blue,
              onPressed: () {},
            ),
          ],
        ),
        decoration: BoxDecoration(
          border: Border.all(
            color: Colors.black,
            width: 1,
          ),
          borderRadius: BorderRadius.circular(12),
        ),
      ),
    );
  }
}
